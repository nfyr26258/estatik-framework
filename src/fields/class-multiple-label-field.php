<?php

namespace EstatikFramework;

/**
 * Class Es_Multiple_Label_Field.
 */
class Es_Framework_Multiple_Label_Field extends Es_Framework_Field {

	/**
	 * @inheritdoc
	 */
	function get_input_markup() {
		$config = $this->set_type( 'hidden' );
		$input = parent::get_input_markup();
		$values = array_filter( explode( ',', $config['value'] ) );
		$items = null;

		if ( ! empty( $values ) ) {
			foreach ( $values as $field_key ) {
				$items .= strtr( $config['item_wrapper'], array(
					'{item_field_key}' => $field_key,
					'{item_label}' => $config['options'][ $field_key ],
				) );
			}
		}

		$input = strtr( $config['items_wrapper'], array(
			'{items}' => $items,
		) );

		$select_field = Es_Framework_Field_Factory::get_field_instance( 'fields', array(
			'type' => 'select',
			'options' => $config['options'],
			'attributes' => array(
				'class' => 'es-field__input js-es-fields-input',
				'placeholder' => __( 'Select field', 'es' ),
			),
		) );

		$select_field = $select_field->get_markup() . "<button class='es-btn es-btn--primary js-es-add-field'>{$config['button_label']}</button>";
		$select_field = sprintf( "<div class='es-fields-list__select'>%s</div>", $select_field );

		$input = $input . $select_field;

		return $input;
	}

	/**
	 * @inheritdoc
	 */
	public function get_default_config() {

		$default = array(
			'button_label' => _x( 'Add' , 'widget button', 'es'),
			'item_wrapper' => "<li class='es-fields-list__item' data-field='{item_field_key}'>
									{item_label}
									<a href='' class='js-es-fields-list__delete es-fields-list__delete'>×</a>
								</li>",
			'items_wrapper' => "<ul class='js-es-fields-list es-fields-list'>{items}</ul>",
		);

		return es_parse_args( $default, parent::get_default_config() );
	}
}
