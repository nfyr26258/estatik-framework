<?php

namespace EstatikFramework;

/**
 * Class Es_Framework_Radio_Field.
 */
class Es_Framework_Switcher_Field extends Es_Framework_Field {

	/**
	 * @return string
	 */
	function get_input_markup() {
		$config = $this->_field_config;

		$this->_field_config['attributes']['type'] = 'checkbox';
		$current_value = $config['value'];

		if ( $current_value == $config['attributes']['value'] ) {
			$this->_field_config['attributes']['checked'] = 'checked';
		}

		$disabled = ! empty( $config['attributes']['disabled'] ) ? 'es-switcher--disabled' : '';

		return sprintf( "
			<input type='hidden' name='" . $config['attributes']['name'] . "' value='0'/>
			<label class='es-switcher {$disabled}'>
				<input %s/>
				<span class='es-switcher-slider es-switcher-slider--round'></span>
			</label>
		", $this->build_attributes_string() );
	}

	/**
	 * Return field default config.
	 *
	 * @return array
	 */
	public function get_default_config() {

		$default = array(
			'attributes' => array(
				'value' => 1,
			),
		);

		return es_parse_args( $default, parent::get_default_config() );
	}
}
