<?php

namespace EstatikFramework;

/**
 * Class Es_Framework_Radio_Boxed_Field.
 */
class Es_Framework_Radio_Boxed_Field extends Es_Framework_Base_Field {

	/**
	 * @return string
	 */
	function get_input_markup() {
		$config = $this->get_field_config();
		$values = ! is_array( $config['value'] ) ? array( $config['value'] ) : $config['value'];
		$values = array_filter( $values );
		$values = empty( $values ) && ! empty( $config['default_value'] ) ? array( $config['default_value'] ) : $values;

		$input = ''; $text = '';
		$type_class = $config['type'] == 'radio-text' ? 'es-box--titled-text' : null;

		if ( ! empty( $config['options'] ) ) {
			foreach ( $config['options'] as $value => $label ) {
				$active = '';
				$input_field = '';
				$field_config = array(
					'type' => 'radio',
					'attributes' => array(
						'type' => 'radio',
						'value' => $value,
						'id' => $config['attributes']['id'] . '-' . $value
					),
					'label' => $label,
					'value' => in_array( $value, $values ) ? $value : '',
				);

				if ( $field_config['value'] == $field_config['attributes']['value'] ) {
					$field_config['attributes']['checked'] = 'checked';
					$active = 'es-box--active';
				}

				$field_config['value'] = $value;

				$field_config = es_parse_args( $field_config, $config );

				$field = new Es_Framework_Field( $this->_field_key, $field_config );

				if ( $config['type'] == 'radio-image' ) {
					if ( ! empty( $config['images'][ $value ] ) ) {
						if ( stristr( $config['images'][ $value ], '<svg' ) ) {
							$input_field .= $config['images'][ $value ];
						} else {
							$input_field .= "<img src='{$config['images'][ $value ]}' width='100' height='auto'/>";
						}
					}
				}

				$input_field .= $field->get_input_markup();

				if ( ! empty( $config['texts'][ $value ] ) ) {
					$text = "<p>{$config['texts'][ $value ]}</p>";
				}

				$input .= strtr( $config['item_wrapper'], array(
					'{input}' => $input_field,
					'{label}' => $label,
					'{active}' => $active,
					'{id}' => $field_config['attributes']['id'],
					'{text}' => $text,
					'{type}' => $type_class,
					'{size}' => $config['size'] ? "es-box--col-{$config['size']}" : '',
					'{item_class}' => $config['item_class'],
				) );
			}
		}

		return strtr( $config['items_wrapper'], array(
			'{items}' => $input,
		) );
	}

	/**
	 * @return array
	 */
	public function get_default_config() {

		// es-radio-image

		$default = array(
			'disable_labels' => false,
			'item_class' => '',
			'size' => 4, // From 1 to 12
			'items_wrapper' => "<div class='es-boxes__list'>{items}</div>",
			'wrapper_class' => "es-field es-field__{field_key} es-field--radio-boxed es-field--{type}",
			'item_wrapper' => "<label class='{size} es-box es-box--input es-box--bordered js-es-box--input {type} {active} {item_class}'>{input}<label class='es-box__title' for='{id}'>{label}</label>{text}</label>",
			'skeleton' => "{before}<div class='{wrapper_class}'>{label}{caption}{input}{description}</div>{after}",
		);

		return es_parse_args( $default, parent::get_default_config() );
	}
}
