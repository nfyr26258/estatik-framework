<?php

namespace EstatikFramework;

/**
 * Class Es_Framework_Base_Field.
 */
abstract class Es_Framework_Base_Field {

	/**
	 * Field simple name for identifier.
	 *
	 * @var string
	 */
	protected $_field_key;

	/**
	 * Fields config array.
	 *
	 * @var array
	 */
	protected $_field_config;

	/**
	 * Es_Framework_Base_Field constructor.
	 *
	 * @param $field_key string
	 * @param $field_config
	 */
	public function __construct( $field_key, $field_config ) {
		$this->_field_key = $field_key;
		$this->_field_config = es_parse_args( $field_config, $this->get_default_config() );
	}

	/**
	 * Set field type.
	 *
	 * @param $type
	 *
	 * @return array
	 */
	public function set_type( $type ) {
		$this->_field_config['type'] = $type;

		return $this->_field_config;
	}

	/**
	 * Return hmtl field markup.
	 *
	 * @return string
	 */
	abstract function get_input_markup();

	/**
	 * Return field skeleton.
	 *
	 * @return mixed
	 */
	public function get_skeleton() {
		return $this->_field_config['skeleton'];
	}

	/**
	 * Return field default config.
	 *
	 * @return array
	 */
	public function get_default_config() {

		return array(
			'label' => '',
			'label_wrapper' => "<div class='es-field__label'>%s</div>",
			'description_wrapper' => "<p class='es-field__description'>%s</p>",
			'caption_wrapper' => "<p class='es-field__caption'>%s</p>",
			'wrapper_class' => "es-field es-field__{field_key} es-field--{type}",
			'attributes' => array(
				'id' => sprintf( "es-field-%s", $this->_field_key ),
				'name' => $this->_field_key,
				'placeholder' => false,
				'class' => 'es-field__input',
			),
			'skeleton' => "{before}<div class='{wrapper_class}'><label for='{id}'>{label}{caption}{input}{description}</label></div>{after}",
			'description' => '',
			'caption' => '',
			'default_value' => '',
			'type' => 'text',
			'value' => '',
			'before' => '',
			'after' => '',
		);
	}

	/**
	 * Return skeleton tokens.
	 *
	 * @return array
	 */
	public function get_tokens() {

		$config = $this->get_field_config();

		if ( ! empty( $config['attributes']['readonly'] ) || ! empty( $config['attributes']['disabled'] ) ) {
			$config['wrapper_class'] .= ' es-field--disabled';
		}

		return apply_filters( 'es_framework_field_tokens', array(
			'{field_key}' => $this->_field_key,
			'{type}' => $config['type'],
			'{id}' => $config['attributes']['id'],
			'{wrapper_class}' => strtr( $config['wrapper_class'], array(
				'{field_key}' => $this->_field_key,
				'{type}' => $config['type'],
				'{id}' => $config['attributes']['id'],
			) ),
			'{input_class}' => $config['attributes']['class'],
			'{label}' => sprintf( $config['label_wrapper'], $config['label'] ),
			'{input}' => $this->get_input_markup(),
			'{description}' => sprintf( $config['description_wrapper'], $config['description'] ),
			'{name}' => $config['attributes']['name'],
			'{caption}' => sprintf( $config['caption_wrapper'], $config['caption'] ),
			'{before}' => $config['before'],
			'{after}' => $config['after'],
		), $this );
	}

	/**
	 * Getter for fields config.
	 *
	 * @return array
	 */
	public function get_field_config() {
		return $this->_field_config;
	}

	/**
	 * Return field markup.
	 *
	 * @return string
	 */
	public function get_markup() {
		return strtr( $this->get_skeleton(), $this->get_tokens() );
	}

	/**
	 * Field render handler.
	 *
	 * @return void
	 */
	public function render() {
		echo $this->get_markup();
	}

	/**
	 * Build input html attributes string.
	 *
	 * @return string
	 */
	public function build_attributes_string() {

		$field_config = $this->get_field_config();
		$options = $field_config['attributes'];

		if ( ! empty( $options['multiple'] ) ) {
			$options['name'] .= '[]';
		}

		$str = '';

		foreach ( $options as $attr_name => $attr_value ) {
			if ( is_string( $attr_value ) || is_numeric( $attr_value ) ) {
				$str .= $attr_name . '=' . '"' . $attr_value . '" ';
			}

			if ( is_array( $attr_value ) ) {
				foreach ( $attr_value as $attr_in_key => $attr_in_value ) {
					$str .= $attr_name . '-' . $attr_in_key . '=' . '"' . $attr_in_value . '" ';
				}
			}
		}

		return apply_filters( 'es_framework_field_attributes_string', $str, $this );
	}
}
