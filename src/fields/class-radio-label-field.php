<?php

namespace EstatikFramework;

/**
 * Class Es_Framework_Radio_Boxed_Field.
 */
class Es_Framework_Radio_Label_Field extends Es_Framework_Base_Field {

	/**
	 * @return string
	 */
	function get_input_markup() {
		$config = $this->get_field_config();
		$values = ! is_array( $config['value'] ) ? array( $config['value'] ) : $config['value'];
		$values = array_filter( $values );
		$values = empty( $values ) && ! empty( $config['default_value'] ) ? array( $config['default_value'] ) : $values;

		$input = '';

		if ( ! empty( $config['options'] ) ) {
			foreach ( $config['options'] as $value => $label ) {
				$active_class = '';

				$field_config = array(
					'type' => 'radio',
					'attributes' => array(
						'type' => 'radio',
						'value' => $value,
						'id' => sprintf( $config['attributes']['id'] . "-%s", $value ),
					),
					'label' => $label,
					'value' => in_array( $value, $values ) ? $value : '',
				);

				if ( $field_config['value'] == $field_config['attributes']['value'] ) {
					$field_config['attributes']['checked'] = 'checked';
					$active_class = 'es-radio-label--active';
				}

				$field_config['value'] = $value;

				$field_config = es_parse_args( $field_config, $config );

				$field = new Es_Framework_Field( $this->_field_key, $field_config );
				$input .= strtr( $config['item_markup'], array(
					'{input}' => $field->get_input_markup(),
					'{label}' => $label,
					'{id}' => $field_config['attributes']['id'],
					'{active}' => $active_class
				) );
			}

			$input = strtr( $config['items_wrapper'], array(
				'{items}' => $input
			) );
		}

		return $input;
	}

	/**
	 * @return array
	 */
	public function get_default_config() {

		$default = array(
			'item_markup' => "<div class='js-es-radio-label es-radio-label {active}'>{input}<label for='{id}'>{label}</label></div>",
			'items_wrapper' => "<div class='es-radio-items__wrapper'>{items}</div>",
		);

		return es_parse_args( $default, parent::get_default_config() );
	}
}
