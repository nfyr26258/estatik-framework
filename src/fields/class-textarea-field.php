<?php

namespace EstatikFramework;

/**
 * Class Es_Framework_Texarea_Field.
 */
class Es_Framework_Textarea_Field extends Es_Framework_Base_Field {

	/**
	 * @return string
	 */
	function get_input_markup() {
		$config = $this->get_field_config();
		return sprintf( "<textarea %s>%s</textarea>", $this->build_attributes_string(), $config['value'] );
	}
}
