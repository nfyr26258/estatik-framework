<?php

namespace EstatikFramework;

/**
 * Class Es_Framework_Select_Field.
 */
class Es_Framework_Select_Field extends Es_Framework_Base_Field {

	/**
	 * @return string
	 */
	function get_input_markup() {
		$config = $this->_field_config;

		// Prepare selectbox values to array.
		$values = ! is_array( $config['value'] ) ? array( $config['value'] ) : $config['value'];

		// Get placeholder string and remove it from input attributes.
		$placeholder = $config['attributes']['placeholder'];
		unset( $config['attributes']['placeholder'] );

		$options = $placeholder ? "<option value=''>$placeholder</option>" : '';

		if ( ! empty( $config['options'] ) ) {
			foreach ( $config['options'] as $value => $label ) {
				$selected = selected( in_array( $value, $values ), true, false );
				$options .= "<option value='{$value}' {$selected}>{$label}</option>";
			}
		}

		return sprintf( "
			<select %s>%s</select>
		", $this->build_attributes_string(), $options );
	}
}
