<?php

namespace EstatikFramework;

/**
 * Class Es_Framework_Field.
 */
class Es_Framework_Field extends Es_Framework_Base_Field {

	/**
	 * @return string
	 */
	function get_input_markup() {
		$this->_field_config['attributes']['type'] = $this->_field_config['type'];
		$this->_field_config['attributes']['value'] = $this->_field_config['value'];
		return sprintf( "<input %s/>", $this->build_attributes_string() );
	}
}
