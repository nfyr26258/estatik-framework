<?php

namespace estatik\framework\fields;

/**
 * Class Es_Framework_Multiple_Checkboxes_Field.
 */
class Es_Framework_Multiple_Checkboxes_Field extends Es_Framework_Base_Field {

	/**
	 * @return string
	 */
	function get_input_markup() {
		$config = $this->get_field_config();
		$values = ! is_array( $config['value'] ) ? array( $config['value'] ) : $config['value'];
		$values = array_filter( $values );
		$values = empty( $values ) && ! empty( $config['default_value'] ) ? $config['default_value'] : $values;
		$values = is_array( $values ) ? $values : array( $values );

		$framework = Es_Framework::get_instance();
		$field_factory = $framework->fields_factory();

		$input = "<input type='hidden' name='{$config['attributes']['name']}' value='0'>";

		if ( ! empty( $config['options'] ) ) {
			foreach ( $config['options'] as $value => $label ) {
				$input_config = array(
					'attributes' => array(
						'value' => $value,
						'name' => $config['attributes']['name'] . "[]",
						'id' => $config['attributes']['id'] . '-' . $value
					),
					'disable_hidden_input' => true,
					'label' => $label,
					'type' => 'checkbox',
					'value' => in_array( $value, $values ) ? $value : '',
				);

				unset( $config['skeleton'] );

				if ( ! empty( $config['checkboxes_config'][ $value ] ) ) {
					$input_config = es_parse_args( $config['checkboxes_config'][ $value ], $input_config );
				}

				$input_config = es_parse_args( $input_config, $config );
				$field = $field_factory::get_field_instance( $this->_field_key, $input_config );
				$input .= $field->get_markup();
			}
		}

		return $input;
	}

	/**
	 * @return array
	 */
	public function get_default_config() {

		$default = array(
			'checkboxes_config' => array(),
		);

		return es_parse_args( $default, parent::get_default_config() );
	}
}
