<?php

namespace EstatikFramework;

/**
 * Class Es_Framework_Radio_Field.
 */
class Es_Framework_Radio_Field extends Es_Framework_Base_Field {

	/**
	 * @return string
	 */
	function get_input_markup() {
		$config = $this->get_field_config();
		$values = ! is_array( $config['value'] ) ? array( $config['value'] ) : $config['value'];
		$values = array_filter( $values );
		$values = empty( $values ) && ! empty( $config['default_value'] ) ? array( $config['default_value'] ) : $values;

		$input = '';

		if ( ! empty( $config['options'] ) ) {
			foreach ( $config['options'] as $value => $label ) {
				$field_config = array(
					'attributes' => array(
						'type' => 'radio',
						'value' => $value,
						'id' => $config['attributes']['id'] . '-' . $value
					),
					'label' => $label,
					'value' => in_array( $value, $values ) ? $value : '',
				);

				if ( $field_config['value'] == $field_config['attributes']['value'] ) {
					$field_config['attributes']['checked'] = 'checked';
				}

				$field_config['value'] = $value;

				$field_config = es_parse_args( $field_config, $config );
				$field_config['skeleton'] = $config['item_skeleton'];

				$field = new Es_Framework_Field( $this->_field_key, $field_config );
				$input .= $field->get_markup();
			}
		}

		return $input;
	}

	/**
	 * Return field default config.
	 *
	 * @return array
	 */
	public function get_default_config() {

		$default = array(
			'item_skeleton' => "<div class='es-radio'>{input}<label for='{id}'>{label}</label></div>"
		);

		return es_parse_args( $default, parent::get_default_config() );
	}
}
