( function( $ ) {
    'use strict';

    function initFields( $context ) {

        $context = $context || $( document );

        $context.find( '.js-es-field__date' ).each( function() {
            var format = $( this ).data( 'date-format' ) || 'm/d/y';
            $( this ).datepicker( {
                dateFormat: format
            } );
        } );
    }

    $( function() {

        $( '.js-es-media-files' ).sortable();

        $( document ).on( 'click', '.js-es-icon-field', function() {
            var $el = $( this );

            $el.closest( '.es-icon-field-wrap' ).toggleClass( 'es-icon-field--active' );
        } );

        $( document ).on( 'click', '.es-file__caption-container, .js-es-file__caption-field', function( e ) {
            e.stopPropagation();
        } );

        $( document ).on( 'click', '.js-es-icon-item', function() {
            var $el = $( this );
            var icon_config = $el.data( 'icon' );
            $el.closest( '.es-icon-field-wrap' )
                .removeClass( 'es-icon-field--active' )
                .find( '.js-es-icon-html' )
                .html( icon_config.icon );

            $el.closest( '.es-icon-field-wrap' ).find( '.es-field__input' ).val( JSON.stringify( icon_config ) );
        } );

        $( 'body' ).on( 'click', function( e ) {
            $( '.es-icon-field-wrap' ).removeClass( 'es-icon-field--active' );
        } );

        $( document ).click( function( e ) {
            //if you click on anything except the modal itself or the "open modal" link, close the modal
            if ( ! $( e.target ).closest( ".es-file__caption-container").length ) {
                $( '.es-file__caption-container.es-file__caption--edit' ).each( function() {
                    var $el = $( this );
                    var $input = $el .find( '.js-es-file__caption-field' );

                    $.post( ajaxurl, {
                        attachment_id: $input.data( 'attachment-id' ),
                        caption: $input.val(),
                        action: 'es_framework_attachment_save_caption',
                        nonce: Es_Framework.nonces.attachment_save_caption_nonce
                    } ).always( function() {
                        $el.removeClass( 'es-file__caption--edit' );

                        if ( $input.val() ) {
                            $el.find( '.js-es-caption' ).html( $input.val() ).removeClass( 'no-caption' );
                        } else {
                            $el.find( '.js-es-caption' ).html( Es_Framework.tr.add_caption ).addClass( 'no-caption' );
                        }
                    } );
                } );
            }
        });

        $( '.es-icons-overlay' ).on( 'click', function(e) {
            e.stopPropagation();
        } );

        // Add repeater item button handler.
        $( document ).on( 'click', '.js-es-repeater__add-item', function() {
            var $el = $( this );
            var $field_wrapper = $el.closest( '.es-field--repeater' );
            var $wrapper = $field_wrapper.find( '.js-es-repeater__wrapper' );
            var index = $wrapper.data( 'index' );
            var $to_clone = $field_wrapper.find( '.js-es-to-clone .js-es-repeater-item' );

            var $cloned = $to_clone.clone();

            $wrapper.find( '.js-es-repeater__items' ).append( $cloned );

            $cloned.find( '[disabled]' ).removeProp( 'disabled' ).closest( '.es-field--disabled' ).removeClass( 'es-field--disabled' );

            $cloned.find( '[for]' ).each( function() {
                $( this ).prop( 'for', $( this ).prop( 'for' ).replace( '{#index}', index ) );
            } );

            $cloned.find( '.es-field__label' ).each( function() {
                $( this ).text( $( this ).text().replace( '{#index}', index + 1 ) );
            } );

            $cloned.find( '[name]' ).each( function() {
                $( this ).prop( 'name', $( this ).prop( 'name' ).replace( '{#index}', index ) );
                $( this ).prop( 'id', $( this ).prop( 'id' ).replace( '{#index}', index ) );
                $wrapper.data( 'index', index + 1 );
            } );

            $cloned.find( 'input:not([type=radio])' ).trigger( 'change' );
            $cloned.find( '.js-es-iris-color-picker:not([disabled])' ).wpColorPicker();
            initFields( $cloned );

            return false;
        } );

        // Remove repeater item button handler.
        $( document ).on( 'click', '.js-es-repeater__delete-item', function() {
            var $el = $( this );
            var $wrapper = $el.closest( '.js-es-repeater__wrapper' );
            var index = $wrapper.data( 'index' );
            $el.closest( '.js-es-repeater-item' ).remove();

            $wrapper.data( 'index', index - 1 );

            $wrapper.find( 'input:not([type=radio])' ).trigger( 'change' );

            return false;
        } );

        // Upload file handler.
        if ( typeof wp !== 'undefined' && wp.media && wp.media.editor ) {
            $( document ).on( 'click', '.js-es-media-button', function () {
                var $button = $( this );
                var $wrapper = $button.parent();
                var $input = $wrapper.find( '.js-es-media-field' );
                var media_item_markup = $button.data( 'item-markup' );
                var is_multiple = $button.data( 'multiple' );
                var item;

                var frame = wp.media( { multiple: is_multiple } );

                frame.open();

                // When an image is selected in the media frame...
                frame.on( 'select', function() {

                    // Get media attachment details from the frame state
                    var attachments = frame.state().get('selection').toJSON();

                    // Attachment IDs array.
                    var ids = [];

                    if ( attachments.length ) {
                        var $files_container = $button.parent().find( '.js-es-media-files' );

                        for ( var i in attachments ) {
                            var file = attachments[i];
                            var filename = file.filename;
                            var url = file.url;
                            var caption = file.caption ? file.caption : false;

                            if ( typeof file.sizes.thumbnail.url !== 'undefined' ) {
                                url = file.sizes.thumbnail.url;
                            }

                            ids.push( attachments[i].id );

                            item = media_item_markup
                                .replace( '{filename}', filename )
                                .replace( '{attachment_id}', file.id )
                                .replace( '{attachment_id}', file.id )
                                .replace( '{attachment_id}', file.id )
                                .replace( '{attachment_id}', file.id )
                                .replace( '{filesize}', file.filesizeHumanReadable )
                                .replace( '{url}', url )
                                .replace( '{url}', url )
                                .replace( '{caption}', caption ? caption : $button.data( 'caption' ) )
                                .replace( '{input_caption}', caption ? caption : '' )
                                .replace( '{no_caption}', caption ? '' : 'no-caption' );

                            if ( $button.data( 'multiple' ) ) {
                                $files_container.append( item );
                                $files_container.sortable();
                            } else {
                                $files_container.html( item );
                            }
                        }
                    }
                } );

                return false;
            } );
        }

        $( document ).on( 'click', '.js-es-caption', function() {
            $( this ).closest( '.es-file__caption-container' ).addClass( 'es-file__caption--edit' );
            return false;
        } );

        $( document ).on( 'click', '.js-es-media-delete-attachment', function() {
            var $el = $( this );
            $el.closest( 'li' ).remove();

            return false;
        } );

        $( document ).on( 'click', '.js-es-tabs .js-es-tabs__nav a.es-tabs__nav-link', function( e ) {
            var $wrapper = $( this ).closest( '.js-es-tabs' );
            var tabContainer = $( this ).attr( 'href' );

            if ( $( tabContainer ).length ) {
                $wrapper.find( '.js-es-tabs__wrapper .js-es-tabs__content' ).addClass( 'es-hidden' );
                $wrapper.find( tabContainer ).removeClass( 'es-hidden' );
                $wrapper.find( '.js-es-tabs__nav li' ).removeClass( 'active' );
                $( this ).closest( 'li' ).addClass( 'active' );

                if ( history.pushState ) {
                    history.pushState( null, null, tabContainer );
                }
                else {
                    location.hash = tabContainer;
                }

                $wrapper.trigger( 'tab_changed', [ tabContainer, $wrapper, $( this ) ] );
            }

            return false;
        } );

        try {
            if ( window.location.hash && $( window.location.hash ).length ) {
                if ( $( window.location.hash ).hasClass( 'js-es-tabs__content' ) ) {
                    $( 'a[href=' + window.location.hash + ']' ).trigger( 'click' );
                }
            }
        } catch( e ) {}


        $( '.js-es-tabs' ).each( function() {
            if ( ! $( this ).find( '.js-es-tabs__nav li.active' ).length ) {
                $( this ).find( '.js-es-tabs__nav li:first-child a.es-tabs__nav-link' ).trigger( 'click' );
            }

            window.scrollTo( 0, 0 );

            setTimeout( function() {
                window.scrollTo( 0, 0 );
            }, 1 );
        } );

        $( '.js-es-iris-color-picker:not([disabled])' ).wpColorPicker();

        $( document ).ajaxComplete( function() {
            $( '.js-es-iris-color-picker' ).wpColorPicker();
        } );

        $( document ).on( 'click', '.js-incrementer-button', function() {
            var method = $( this ).data( 'method' );
            var $input = $( this ).closest( '.js-es-incrementer-field' ).find( 'input[type=number]' );
            var max = $input.prop( 'max' );
            var min = $input.prop( 'min' );
            var step = + $input.prop( 'step' );
            step = step || 1;
            var val = $input.val() ? $input.val() : 0;

            if ( 'increment' === method ) {
                $input.val( ( max && max > val ) || max === '' ? + val + step : val );
            } else {
                $input.val( ( min && min < val ) || min === '' ? + val - step : val );
            }

            $input.trigger( 'change' );

            return false;
        } );

        $( '.js-es-radio-label input[type=radio]' ).change( function() {
            var $wrap = $( this ).closest( '.js-es-radio-label' ).parent();
            $wrap.find( '.js-es-radio-label' ).removeClass( 'es-radio-label--active' );

            if ( $( this ).is( ':checked' ) ) {
                $( this ).closest( '.js-es-radio-label' ).addClass( 'es-radio-label--active' );
            }
        } );

        $( document ).on( 'change', '.js-es-box--input input[type=radio]', function() {
            var $wrap = $( this ).closest( '.es-boxes__list' );
            $wrap.find( '.js-es-box--input' ).removeClass( 'es-box--active' );

            console.log($wrap.find( 'input[type=radio]:checked' ).closest( '.es-boxes__list' ));

            $wrap.find( 'input[type=radio]:checked' ).closest( '.js-es-box--input' ).addClass( 'es-box--active' );
        } ).trigger( 'change' );

        $( document ).on( 'change', 'input[data-toggle-container], input[data-inactive-container]',  function() {
            var $el = $( this );
            var container = $el.data( 'toggle-container' );

            if ( container && container.length ) {
                var $container = $( container );
                if ( $el.is( ':checked' ) ) {
                    $container.removeClass( 'es-hidden' );
                } else {
                    $container.addClass( 'es-hidden' );
                }
            } else {
                var $active_container = $( $el.data( 'active-container' ) );
                var $inactive_container = $( $el.data( 'inactive-container' ) );

                if ( $el.is( ':checked' ) ) {
                    $active_container.removeClass( 'es-hidden' );
                    $inactive_container.addClass( 'es-hidden' );

                    $active_container.find( 'input,select,textarea' ).removeProp( 'disabled' );
                    $inactive_container.find( 'input,select,textarea' ).prop( 'disabled', 'disabled' );
                } else {
                    $active_container.addClass( 'es-hidden' );
                    $inactive_container.removeClass( 'es-hidden' );

                    $active_container.find( 'input,select,textarea' ).prop( 'disabled', 'disabled' );
                    $inactive_container.find( 'input,select,textarea' ).removeProp( 'disabled' );
                }
            }
        } );

        $( 'input[data-toggle-container], input[data-inactive-container]' ).trigger( 'change' );

        $( document ).on( 'click', '[data-toggle-container]:not(input)', function() {
            $( $( this ).data( 'toggle-container' ) ).toggleClass( 'es-hidden' );
            return false;
        } );

        $( document ).on( 'click', '.js-es-add-field', function() {
            return false;
        } );
    } );
} )( jQuery );
