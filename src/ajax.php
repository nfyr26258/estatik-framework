<?php

add_action( 'wp_ajax_es_framework_attachment_save_caption', 'es_framework_ajax_save_attachment_caption' );

/**
 * Update image caption.
 *
 * @return void
 */
function es_framework_ajax_save_attachment_caption() {

	$action = 'es_framework_attachment_save_caption';

	if ( check_ajax_referer( $action, 'nonce' ) ) {
		$post_id = intval( filter_input( INPUT_POST, 'attachment_id' ) );
		$caption = sanitize_text_field( filter_input( INPUT_POST, 'caption' ) );
		$attachment = get_post( $post_id );

		if ( $post_id && ! empty( $attachment->post_type ) && $attachment->post_type == 'attachment'  ) {
			if ( current_user_can( 'edit_post', $post_id ) ) {
				wp_update_post( array(
					'ID' => $post_id,
					'post_excerpt' => $caption,
				) );
			}
		}
	}

	wp_die();
}