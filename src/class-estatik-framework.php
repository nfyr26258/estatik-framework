<?php

namespace estatik\EstatikFramework;

/**
 * Class Es_Framework.
 */
class Es_Framework {

	/**
	 * Framework instance.
	 *
	 * @var Es_Framework
	 */
	protected static $_instance;

	/**
	 * Estatik constructor.
	 *
	 * @return void
	 */
	public function __construct() {

	}

	/**
	 * @return void
	 */
//	public function load_assets() {
//		$url = plugin_dir_url( __FILE__ ) . 'assets' . DS;
//
//		wp_enqueue_script( 'es-framework', $url . 'js' . DS . 'framework.js', array( 'jquery', 'wp-color-picker', 'jquery-ui-datepicker', 'jquery-ui-sortable' ) );
//		wp_localize_script( 'es-framework', 'Es_Framework', array(
//			'nonces' => array(
//				'attachment_save_caption_nonce' => wp_create_nonce( 'es_framework_attachment_save_caption' ),
//			),
//			'tr' => array(
//				'add_caption' => __( 'Add caption', 'es' ),
//			)
//		) );
//		wp_enqueue_style( 'es-framework', $url . 'css' . DS . 'framework.css' );
//	}

	/**
	 * Return fields renderer instance.
	 *
	 * @param $fields_config
	 *
	 * @return Es_Framework_Fields_Renderer
	 */
	public function fields_renderer( $fields_config ) {
		return apply_filters( 'es_framework_fields_renderer', new Es_Framework_Fields_Renderer( $fields_config, $this ) );
	}

	/**
	 * Return fields widget renderer instance.
	 *
	 * @param $fields_config
	 * @param $fields_data
	 * @param $widget_instance
	 *
	 * @return Es_Framework_Widget_Fields_Renderer
	 */
	public function widget_fields_renderer( $fields_config, $fields_data, $widget_instance ) {
		return apply_filters( 'es_framework_widget_fields_renderer', new Es_Framework_Widget_Fields_Renderer( $fields_config, $fields_data, $widget_instance, $this ) );
	}

	/**
	 * Return fields factory instance.
	 *
	 * @return Es_Framework_Field_Factory
	 */
	public function fields_factory() {
		return apply_filters( 'es_framework_fields_factory', new Es_Framework_Field_Factory() );
	}

	/**
	 * Views factory.
	 *
	 * @return Es_Framework_View_Factory
	 */
	public function views_factory() {
		return apply_filters( 'es_framework_views_factory', new Es_Framework_View_Factory() );
	}

	/**
	 * Return plugin instance.
	 *
	 * @return Es_Framework
	 */
	public static function get_instance() {

		if ( ! static::$_instance ) {
			static::$_instance = new static();
		}

		return static::$_instance;
	}

	/**
	 * @return string
	 */
	public static function get_path() {
		return plugin_dir_path( __FILE__ );
	}
}
